# README #

## No Need to Install Python or other dependencies. Simply Run Downloader from the terminal/cmd.Supports 64bit Linux OS and 64bit Windows OS##

### *downloader - Linux OS* 

### *downloader.exe - Windows OS* ###

**Download cu_pdf_downloader from [Downloads](https://bitbucket.org/shaiju1983/cu_pdf_downloader/downloads) of Bitbucket.**

**Next Run downloader as shown below:**

*Linux*
```
./downloader
```
*Windows*
```
downloader.exe
```

**If successful, a folder named "SEMX_DD-MM-YYYY" will be created with downloaded pdf files.**

***After downloading the pdf,for result analysis use [analyzer](https://bitbucket.org/shaiju1983/sm-result-analysis)***

*Someone please refactor this code !!!*
--------------------------------------------